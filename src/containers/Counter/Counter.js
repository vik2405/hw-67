import React, {Component} from "react";
import './Counter.css';
import {connect} from 'react-redux';
import actionTypes from "../../store/actionTypes";
class Counter extends Component {
    render() {
        return (
            <div className="Counter">
                <h1 className="title">{this.props.scoreboard}</h1>
                <h2>{this.props.hello}</h2>
                <button onClick={() => this.props.addNumber(1)}>1</button>
                <button onClick={() => this.props.addNumber(2)}>2</button>
                <button onClick={() => this.props.addNumber(3)}>3</button>
                <button onClick={() => this.props.addNumber(4)}>4</button>
                <button onClick={() => this.props.addNumber(5)}>5</button>
                <button onClick={() => this.props.addNumber(6)}>6</button>
                <button onClick={() => this.props.addNumber(7)}>7</button>
                <button onClick={() => this.props.addNumber(8)}>8</button>
                <button onClick={() => this.props.addNumber(9)}>9</button>
                <button onClick={() => this.props.addNumber(0)}>0</button>
                <button onClick={() => this.props.addNumber('+')}>+</button>
                <button onClick={() => this.props.addNumber('-')}>-</button>
                <button onClick={() => this.props.delNumber('')}>C</button>
                <button onClick={() => this.props.resultNumber('')}>E</button>
                <button onClick={() => this.props.remNumber('')}>R</button>

            </div>
        );
    }
}
  const mapStateToProps = state => {
    return {
        scoreboard: state.counter,
    }
};
  const mapDispatchToProps = dispatch => {
    return {
        addNumber: (number) => dispatch({type: actionTypes.NUMBER_TYPE, number}),
        delNumber: (number) => dispatch({type: actionTypes.DEL_NUMBER, number}),
        resultNumber:(number) => dispatch({type: actionTypes.RESULT_NUMBER, number}),
        remNumber: (number) => dispatch({type: actionTypes.REM_NUMBER, number})
    };
};

     export default connect(mapStateToProps,mapDispatchToProps)(Counter);