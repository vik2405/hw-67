
import actionTypes from "./actionTypes";
const initialState = {
    counter: ''
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case  actionTypes.NUMBER_TYPE:
            return {counter:state.counter + action.number};
        case  actionTypes.DEL_NUMBER:
            return {counter:state.counter.slice(0,-1)};
        case  actionTypes.RESULT_NUMBER:
            return  {counter: String(eval(state.counter))};
        case  actionTypes.REM_NUMBER:
            return  {counter:state.counter.slice(0,-5)};
        default:
            return state
    }
};
export default reducer;

